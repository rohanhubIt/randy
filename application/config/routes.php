<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Login';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'Login';
$route['admin/dashboard'] = 'welcome/uiDashboard';
$route['admin/raw-material'] = 'welcome/uiRawMaterial';
$route['admin/supplier'] = 'welcome/uiSupplier';
$route['admin/customer'] = 'welcome/uiCustomer';
$route['admin/production'] = 'welcome/uiProduction';
$route['admin/design'] = 'welcome/uiDesign';
$route['admin/sales-pos'] = 'welcome/uiSalesPos';
$route['admin/sales-credit'] = 'welcome/uiSalesCredit';
$route['admin/cancelled-invoices'] = 'welcome/uiCancelledInvoices';

$route['admin/production'] = 'welcome/uiProduction';
$route['admin/complete-items'] = 'welcome/uiCompleteItems';

$route['admin/delivery-note'] = 'welcome/uiDeliveryNote';
$route['admin/return-note'] = 'welcome/uiReturnNote';
$route['admin/equipment'] = 'welcome/uiEquipment';
$route['admin/grn'] = 'welcome/uiGrn';
$route['admin/product'] = 'welcome/uiProducts';

$route['admin/categories'] = 'welcome/uiCategories';
$route['admin/units'] = 'welcome/uiUnits';
$route['admin/issue-note'] = 'welcome/issueNot';
$route['admin/sales-note'] = 'welcome/salesNote';

$route['admin/product/(:any)'] = 'welcome/uiProducts';

$route['admin/import-csv'] = 'welcome/importCSV';



// reports url 

$route['admin/issue-report'] = 'reports/issueReport';
$route['admin/sales-report'] = 'reports/salesReport';
$route['admin/stock-report'] = 'reports/stockReport';
$route['admin/profitability-report'] = 'reports/profitibilityReport';
$route['search-profit-report'] = 'reports/profitibilityReportSearch';
// reports url

$route['save/production'] = 'master/saveProduction';

$route['save/grn'] = 'master/saveGRN';

$route['user/user-registration'] = 'welcome/userRegistration';

$route['auth/action'] = 'Login/loginAction';

$route['save/user'] = 'master/saveRegistration';


$route['save/save-category'] = 'master/saveCategory';
$route['save/save-unit'] = 'master/saveUnit';

// customer form
$route['save/customer'] = 'master/saveCustomer';

//supplier form
$route['save/supplier'] = 'master/saveSupplier';

//Raw material form
$route['save/rawmaterial'] = 'master/saveRawmaterial';

//product save form
$route['save/product'] = 'master/saveProduct';

$route['edit/product'] = 'master/updateProducts';

//POS cash sale save form 
$route['save/poscash']='sales/savePoscash';


$route['save/delevery'] = 'sales/Delevery';

$route['save/issue-note'] = 'master/saveIssueNote';
$route['save/sales-note'] = 'master/saveSalesNote';

$route['save/import_csv'] = 'master/saveCSV';

// view issue note, sales note
$route['view-issue-note/(:any)'] = 'welcome/viewIssueNote';
$route['view-sales-note/(:any)'] = 'welcome/viewSalesNote';


$route['logout'] = 'welcome/logout';

//sales credit save form

// edit forms

$route['admin/edit-raw-material/(:any)'] = 'welcome/uiRawMaterial';
$route['save/update-rawmaterial/(:any)'] = 'master/updateRawmaterial';
$route['save/disable-raw-material/(:any)/(:any)'] = 'master/disableRawmaterial';


$route['admin/edit-supplier/(:any)'] = 'welcome/uiSupplier';
$route['save/update-supplier/(:any)'] = 'master/updateSupplier';
$route['save/disable-supplier/(:any)/(:any)'] = 'master/disableSupplier';

$route['admin/edit-customer/(:any)'] = 'welcome/uiCustomer';
$route['save/update-customer/(:any)'] = 'master/updateCustomer';
$route['save/disable-customer/(:any)/(:any)'] = 'master/disableCustomer';

$route['admin/edit-product/(:any)'] = 'welcome/uiProducts';
$route['save/update-product/(:any)'] = 'master/updateProducts';
$route['save/disable-product/(:any)/(:any)'] = 'master/disableProducts';


$route['save/equipment'] = 'master/saveEquipment';
$route['admin/edit-equipment/(:any)'] = 'welcome/uiEquipment';
$route['save/update-equipment/(:any)'] = 'master/updateEquipment';
$route['save/disable-equipment/(:any)/(:any)'] = 'master/disableEquipment';

// edit forms
$route['']='';
