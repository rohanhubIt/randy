<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>View Issue Note
	        <a href="javascript:void(0)" class="btn btn-success" onclick="window.print()">
	        	<i class="fa fa-print" aria-hidden="true" ></i>
	        	Print
	        </a>
        </h3>

      </div>


	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>



      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">

            <div class="x_content">
                <div class="row">
                  <div class="col-md-1 col-sm-1">
                <img style="width: 100px" src="<?php echo site_url('assets/build/images/logo.png') ?>">
                    <br><br>
                    <p>No.4, Temple Road<br>
                    Moratumulla, Moratuwa</p>
                  </div>
                  <div class="col-md-5 col-sm-6" style="width: 420px;text-align: center;">
                    <h1 style="font-size: 24px;">ASANKA HARDWARE</h1>
                    <h5>
                      N/C Thinner, Retarder, Lacquer, Sanding Seaker, Water Base, Tools,<br>
                      Hardware Items, Wholesale & Retail Distributors.
                    </h5>
                    <p>Tel : 011 2 652 436, 011 5 235 237 Hotline : 077 99 53 535</p>
                  </div>
                  <div class="col-md-1 col-sm-1">
                    <img style="width: 70px;" src="<?php echo site_url('assets/build/images/asian_paints.png') ?>">
                    <br><br>
                    <img style="width: 70px;" src="<?php echo site_url('assets/build/images/jat.png') ?>">
                    <br><br>
                    <img style="width: 70px;" src="<?php echo site_url('assets/build/images/sayerlack.png') ?>">
                  </div>
                </div>
                
            </div>

          </div>
          </div>
      </div>
    </div>



	    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Issue Note</h2>
                <ul class="nav navbar-right panel_toolbox">

                </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br>

                <div class="row">
                  <div class="col-md-3">
                    Issue Note No
                  </div>

                  <div class="col-md-3">
                  <?php 
                    $code = str_pad($issue_note[0]->issue_note_id, 4, '0', STR_PAD_LEFT);
                    echo  "INT_".$code;
                  ?>
                  </div>

                  <div class="col-md-3">
                    Date
                  </div>

                  <div class="col-md-3">
                    <?php echo $issue_note[0]->issue_date ?>
                  </div>
                </div>

                <!-- sec row -->
                <div class="row">
                  <div class="col-md-3">
                    From - <?php echo ($issue_note[0]->from_stock == "1")? "Ground": "Moving";  ?>
                  </div>
                  <div class="col-md-3">
                    To - <?php echo ($issue_note[0]->to_stock == "1")? "Ground": "Moving";  ?>
                  </div>
                  <div class="col-md-6">
                    Remark - <?php echo ($issue_note[0]->remark == "")? "N/A": $issue_note[0]->remark;  ?>
                  </div>
                </div><br>

                <!-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -->
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th>Product Id</th>
                      <th>Product</th>
                      <th>Qty</th>
                      <th>Price</th>
                      <th>Sub Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $grndTotal = 0; ?>
                    <?php foreach ($issue_noteDetail as $key => $value): ?>
                      <tr>
                        <td><?php echo $value->pro_id ?></td>
                        <td><?php echo $value->pro_code." - ".$value->guage." - ".$value->pro_name." - ".$value->Description; ?></td>
                        <td><?php echo $value->qty ?></td>
                        <td><?php echo $value->selling_price ?></td>
                        <td><?php echo $value->sub_total ?></td>
                      </tr>
                      <?php $grndTotal += $value->sub_total; ?>
                    <?php endforeach ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td>Grand Total</td>
                      <td><?php echo $grndTotal; ?></td>
                    </tr>
                  </tfoot>
                </table>


               </div>
          </div>
          </div>
          </div>
    	</div>

      <div class="row">
        <div class="col-md-12">
          <div style="width: 700px">
            <h6 style="text-align: center;">Asanka Hardware Invenotry System</h6>
            <h6 style="text-align: center;">Developed by Web Dyno Solutions (077 197 5806)</h6>
          </div>
        </div>
      </div>

  </div>
</div>
</div>