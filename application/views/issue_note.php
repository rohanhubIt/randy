<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Issue Note</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Issue Note</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<div class="x_content">

<form action="<?php echo site_url('save/issue-note') ?>" method="post" class="form-horizontal form-label-left">

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date<span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" name="date" required="required" class="form-control date-pic col-md-7 col-xs-12" value="<?php echo date('Y-m-d') ?>">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">From<span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="form-control" name="from_stock">
            <option value="">select</option>
            <option value="1">Ground Stocks</option>
            <option value="2">Moving Stocks</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">To<span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <select class="form-control" name="to_stock">
            <option value="">select</option>
            <option value="1">Ground Stocks</option>
            <option value="2">Moving Stocks</option>
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Remark
          <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <textarea name="remark" value="" class="form-control">
              
            </textarea>
        </div>
      </div>

      <table class="table table-bordered">
        <thead>
          <tr>
            <th>
              Product Id
            </th>
            <th>
              Qty
            </th>
            <th>
              Selling Price
            </th>
            <th>
              Sub Total
            </th>
            <th>
              Actions
            </th>
          </tr>
        </thead>
        <tbody id="tbody">
          <tr>
            <td>
              <select id="pro_id" class="form-control">
                <option value="">select</option>
                <?php foreach ($product as $key => $value): ?>
                  <option value="<?php echo $value->pro_id ?>" 
                    sell-price="<?php echo $value->price ?>"
                    buy-price="<?php echo $value->cost ?>"
                    available-qty="
                    <?php 
                    
                      $CI =& get_instance();
                      $details = $CI->Reports_model->selectAvaliableStock($value->pro_id);
                      echo $details[0]->qty;
                    ?>
                    "
                  >
                    <?php echo $value->pro_code." - ".$value->guage." - ".$value->pro_name." - ".$value->Description; ?>
                  </option>
                <?php endforeach ?>
              </select>
            </td>
            <td>
              <input id="qty"  type="number" class="form-control" name="">
            </td>
            <td>
              <input id="selling_price" type="text" class="form-control" name="">
            </td>
            <td>
              <input id="sub_total" type="text" readonly="" class="form-control" name="">
            </td>
            <td>
              <button id="issue-note-add" class="btn btn-info" type="button">Add</button>
            </td>                                    
          </tr>


        </tbody>
        <tfoot>
          <tr>
            <td>Grand Total</td>
            <td id="grd_total">N/A</td>
          </tr>
        </tfoot>
      </table>

<hr>
<div class="col-md-3 col-md-offset-9">
  <button type="submit" class="btn btn-success">Issue</button>
  <button type="reset" class="btn btn-danger">Cancel</button> 
</div>


</form>
</div>

				</div>
			</div>
		</div>



    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Issue Note Table</h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
 
                 <table class="table table-bordered common_grid">
                  <thead>
                    <tr>
                      <th>Note No</th>
                      <th>Remark</th>
                      <th>Date</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($table_data as $key => $value): ?>
                      <tr>
                        <td>
                          <?php 
                            $code = str_pad($value->issue_note_id, 4, '0', STR_PAD_LEFT);
                            echo  "INT_".$code;
                          ?>
                        </td>
                        <td>
                          <?php echo ($value->remark == "")? "N/A": $value->remark;  ?>
                        </td>
                        <td>
                          <?php echo ($value->issue_date == "")? "N/A": $value->issue_date;  ?>
                        </td>
                        <td>
                          <?php echo ($value->from_stock == "1")? "Ground": "Moving";  ?>
                        </td>
                        <td>
                          <?php echo ($value->to_stock == "1")? "Ground": "Moving";  ?>
                        </td>
                        <td>

                          <a href="<?php echo site_url('view-issue-note/').$value->issue_note_id; ?>" class="btn btn-info btn-xs">
                          View
                          </a>
                          
                        </td>                                            
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>



               </div>
          </div>
          </div>
          </div>
    </div>


  </div>
</div>