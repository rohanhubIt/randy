        <!-- footer content -->
        <footer>
          <div class="pull-left">
            Inventory System by <a href="">Web dyno Solutions </a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- Bootstrap -->
    <script src="<?php echo site_url() ?>assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo site_url() ?>assets/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo site_url() ?>assets/vendors/nprogress/nprogress.js"></script>


    <script src="<?php echo site_url() ?>assets/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/pdfmake/build/vfs_fonts.js"></script>
    

    <script src="<?php echo site_url() ?>assets/vendors/jquery-ui/jquery-ui.min.js"></script>

    <!-- js validation -->

    <script src="<?php echo site_url() ?>assets/jquery-validation/dist/jquery.validate.min.js"></script>
    <script src="<?php echo site_url() ?>assets/jquery-validation/jquery_custom_methods.js"></script>


    <!-- js validation -->

    <!-- Custom Theme Scripts -->
    <script src="<?php echo site_url() ?>assets/build/js/custom.min.js"></script>

    <script type="text/javascript">

        var grdTotl = 0;

        $( ".date-pic" ).datepicker({
            altFormat: "yyyy-mm-dd",
            dateFormat: "yy-mm-dd",
            minDate: "0"
        });
        
        $( ".date-pic-all" ).datepicker({
            altFormat: "yyyy-mm-dd",
            dateFormat: "yy-mm-dd",
        });

        $("#issue-note-add").on("click", function() {
            var pro_id = $("#pro_id :selected").val();
            var pro_name = $("#pro_id :selected").text();

            var qty = $("#qty").val();
            var selling_price = $("#selling_price").val();
            var sub_total = $("#sub_total").val();

            var pro_a_qty = $('#pro_id option:selected').attr('available-qty');

            qty = parseInt(qty);
            pro_a_qty = parseInt(pro_a_qty);
            
            //if(pro_a_qty < qty) {
                //alert("Qty Unavailable, please check invenotry");
           // } else {

                var html = "";

                html += "<tr id='issue_row"+pro_id+"'>";
                    // product
                    html += "<td>";
                        html += ""+pro_name;
                        html += "<input type='hidden' name='line_pro_id[]' value='"+pro_id+"'>";
                    html += "</td>";

                    // qty
                    html += "<td>";
                        html += ""+qty;
                        html += "<input type='hidden' name='line_qty[]' value='"+qty+"'>";
                    html += "</td>";

                    // selling price
                    html += "<td>";
                        html += ""+selling_price;
                        html += "<input type='hidden' name='selling_price[]' value='"+selling_price+"'>";
                    html += "</td>";                

                    // selling price
                    html += "<td>";
                        html += ""+sub_total;
                        html += "<input type='hidden' name='sub_total[]' value='"+sub_total+"'>";
                    html += "</td>"; 

                    // sub total
                    html += "<td>";
                        html += "<a href='javascript:void(0)' class='btn btn-danger btn-xs issue-note-item-rm' data-id='"+pro_id+"'>Remove</a>";
                    html += "</td>"; 

                    grdTotl += parseFloat(sub_total);

                    html += "</tr>";

                    $("#tbody").append(""+html);
                    $("#grd_total").html(""+grdTotl);


                    $("#qty").val('');
                    $("#selling_price").val('');
                    $("#sub_total").val('');

                    $("#pro_id option:selected").removeAttr("selected");


            //}

        });

        $("#selling_price").on("keyup", function() {
            var qty = $("#qty").val();
            var selling_price = $("#selling_price").val();

            var total = parseFloat(qty) * parseFloat(selling_price);
            $("#sub_total").val(""+total);
        });

// ----------------------------------------------------------------------------------------------\\
// ------------------------------------------ sales note ----------------------------------------\\
// -----------------------------------------------------------------------------------------------\\

        $("#sale-add").on("click", function() {
            var pro_id = $("#sale-pro-id :selected").val();
            var pro_name = $("#sale-pro-id :selected").text();

            var qty = $("#sale-qty").val();
            var selling_price = $("#sale-selling-price").val();
            var sub_total = $("#sale-sub-total").val();



            var pro_a_qty = $('#sale-pro-id option:selected').attr('available-qty');

            qty = parseInt(qty);
            pro_a_qty = parseInt(pro_a_qty);
            
            if(pro_a_qty < qty) {
                alert("Qty Unavailable, please check invenotry");
            } else {
               
                var html = "";

                html += "<tr id='sell_row"+pro_id+"'>";
                    // product
                    html += "<td>";
                        html += ""+pro_name;
                        html += "<input type='hidden' name='line_pro_id[]' value='"+pro_id+"'>";
                    html += "</td>";

                    // qty
                    html += "<td>";
                        html += ""+qty;
                        html += "<input type='hidden' name='line_qty[]' value='"+qty+"'>";
                    html += "</td>";

                    // selling price
                    html += "<td>";
                        html += ""+selling_price;
                        html += "<input type='hidden' name='selling_price[]' value='"+selling_price+"'>";
                    html += "</td>";                

                    // selling price
                    html += "<td>";
                        html += ""+sub_total;
                        html += "<input type='hidden' name='sub_total[]' value='"+sub_total+"'>";
                    html += "</td>"; 

                    // sub total
                    html += "<td>";
                        html += "<a href='javascript:void(0)' class='btn btn-danger btn-xs sell-item-remove' data-id='"+pro_id+"'>Remove</a>";
                    html += "</td>"; 

                    grdTotl += parseFloat(sub_total);

                html += "</tr>";

                $("#tbody").append(""+html);
                $("#grd_total").html(""+grdTotl);
                $("#grnd_total").val(""+grdTotl);


                $("#sale-qty").val('');
                $("#sale-selling-price").val('');
                $("#sale-sub-total").val('');

                $("#sale-pro-id option:selected").removeAttr("selected");   


                var discount = $("#discount").val();

                var total = parseFloat(grdTotl) - parseFloat(discount);

                $("#payment").html(""+total);                            
                
            }

        });

        // selling items remove
        $("body").on("click", ".sell-item-remove", function() {
            var id = $(this).attr("data-id");
            console.log(""+id);
            $("#sell_row"+id).remove();
        });
        // selling items remove

        // issue items remove
        $("body").on("click", ".issue-note-item-rm", function() {
            var id = $(this).attr("data-id");
            console.log(""+id);
            $("#issue_row"+id).remove();
        });
        // issue items remove

        $("#sale-selling-price").on("keyup", function() {
            var qty = $("#sale-qty").val();
            var selling_price = $("#sale-selling-price").val();

            var total = parseFloat(qty) * parseFloat(selling_price);
            $("#sale-sub-total").val(""+total);
        });

        $("#discount").on("keyup", function() {
            var grdTotl = $("#grd_total").text();
            var discount = $(this).val();

            var total = parseFloat(grdTotl) - parseFloat(discount);

            $("#payment").html(""+total);
        });
    </script>


<!--    Common Success message -->
<?php
if ($this->session->flashdata('msg')) {
    $message = $this->session->flashdata('msg');
    echo "<script>alert('" . $message . "');</script>";
} else if ($this->session->flashdata('msg_error')) {
    $message = $this->session->flashdata('msg_error');
    echo "<script>alert('" . $message . "');</script>";
}
?>

</body>
</html>