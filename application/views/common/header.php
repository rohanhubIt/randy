<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Inventory System</title>

    <!-- Bootstrap -->
    <link href="<?php echo site_url() ?>assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo site_url() ?>assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo site_url() ?>assets/vendors/nprogress/nprogress.css" rel="stylesheet">

    <link href="<?php echo site_url() ?>assets/vendors/jquery-ui/jquery-ui.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo site_url() ?>assets/build/css/custom.min.css" rel="stylesheet">

    <style type="text/css">
      .thumbnail {
        text-align: center;
        padding: 30px;
      }

      .thumbnail i {
        font-size: 60px;
      }

      .thumbnail h4 {
            margin-top: 15px;
      }
    </style>


    <!-- jQuery -->
    <script src="<?php echo site_url() ?>assets/vendors/jquery/dist/jquery.min.js"></script>

    <!-- Datatables -->
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo site_url() ?>assets/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>

    
  </head>

  <body class="nav-md">

<?php if($this->session->has_userdata('client_auth')): ?>  
  <?php
    $userdata = $this->session->userdata('client_auth');
    $userdata = json_decode($userdata);
  ?>
<?php endif; ?>

  <div class="container body">
  <div class="main_container">

        <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="" class="site_title"> <span>Inventory</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo $userdata[0]->full_name ?></h2>
              </div>
              <div class="clearfix"></div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General Options</h3>
                <ul class="nav side-menu">
                  

                  <li>
                    <a href="<?php echo site_url(); ?>admin/dashboard">
                      <i class="fa fa-home"></i> Home
                    </a>
                  </li>

<?php if ($userdata[0]->type == "1"): ?>
 <li>
  <a>
    <i class="fa fa-edit"></i> Master File
    <span class="fa fa-chevron-down"></span>
  </a>
                    <ul class="nav child_menu">
<!--                       <li><a href="<?php // echo site_url() ?>admin/raw-material">Raw Material</a></li>
                      <li><a href="<?php // echo site_url() ?>admin/supplier">Supplier</a></li> -->
                      <li><a href="<?php echo site_url() ?>admin/customer">Customer</a></li>
                      <li><a href="<?php echo site_url() ?>admin/categories">Categories</a></li>
                      <li><a href="<?php echo site_url() ?>admin/product">Product</a></li>
                      <li><a href="<?php echo site_url() ?>admin/units">Units</a></li>
<!--                       <li><a href="<?php // echo site_url() ?>admin/equipment">Equipment</a></li> -->
                    </ul>
                  </li>

<!--                   <li><a><i class="fa fa-desktop"></i> Production Mgmt <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php // echo site_url(); ?>admin/grn">GRN</a></li>
                      <li><a href="<?php // echo site_url(); ?>admin/production">Production</a></li>
                    </ul>
                  </li> -->

                  <li><a><i class="fa fa-table"></i> Inventory Mgmt <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li>
                        <a href="<?php echo site_url('admin/issue-note') ?>">Issue Note</a>
                        <a href="<?php echo site_url('admin/sales-note') ?>">Sales Note</a>
                      </li>
                    </ul>
                  </li>

                  <li><a><i class="fa fa-bar-chart-o"></i>Reports <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('admin/issue-report') ?>">Issue Report</a></li>
                      <li><a href="<?php echo site_url('admin/sales-report') ?>">Sales Report</a></li>
                      <li><a href="<?php echo site_url('admin/stock-report') ?>">Stock Report</a></li>
                      <li><a href="<?php echo site_url('admin/profitability-report') ?>">Profitability Report</a></li>
                    </ul>
                  </li>

                </ul> 
<?php endif ?>
                  
              </div>
              <div class="menu_section">
                <?php if ($userdata[0]->type == "1"): ?>
                <h3>Advanced Options</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-user"></i> User Management <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      
                      <li>
                        <a href="<?php echo site_url(); ?>user/user-registration">User Registration</a>
                      </li>
                      
                    </ul>
                  </li>

                </ul>
              <?php endif ?>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
             <!--  <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a> -->
              <!-- <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a> -->
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

         <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo site_url() ?>assets/build/images/img.jpg" alt="">
                    <?php echo $userdata[0]->full_name ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;">Help</a></li>
                    <li><a href="<?php echo site_url('logout') ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
       