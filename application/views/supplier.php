<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Supplier Registration</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>

              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Supplier form </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
	             <div class="x_content">
        
                

                    <br />

                  <?php if($form_data== null): ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo site_url('save/supplier') ?>">
                  <?php else: ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo site_url('save/update-supplier/'.$form_data[0]->sup_id) ?>">
                  <?php endif; ?>


                    

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">First Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="first-name" name="f_name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($form_data != null)? $form_data[0]->first_name: '';?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Last Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="last-name" name="l_name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($form_data != null)? $form_data[0]->last_name: '';?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input id="middle-name" class="form-control col-md-7 col-xs-12" type="text" name="address" value="<?php echo ($form_data != null)? $form_data[0]->address: '';?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div id="gender" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="gender" value="1" <?php echo ($form_data != null && $form_data[0]->gender == '1')?'checked':''; ?>> &nbsp; Male &nbsp;
                            </label>
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="gender"  value="0"  <?php echo ($form_data != null && $form_data[0]->gender == "0")?'checked':''; ?>> Female
                            </label>
                          </div>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						              <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>

 
          </div>
          </div>
          </div>
    </div>


    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Supplier table </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
                <table class="table table-bordered common_grid">
                  <thead>
                    <tr>
                      <th>Supplier Id</th>
                      <th>First Name</th>
                      <th>Last Name</th>
                      <th>Address</th>
                      <th>Gender</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($table_data as $key => $value): ?>
                      <tr>
                        <td><?php echo $value->sup_id; ?></td>
                        <td><?php echo $value->first_name; ?></td>
                        <td><?php echo $value->last_name; ?></td>
                        <td><?php echo $value->address; ?></td>
                        <td><?php echo ($value->gender == "1")? "Male": "Female"; ?></td>
                        <td><?php echo ($value->status == "1")?"Active":"Deactive"; ?></td>
                        <td>
                          
                          <a href="<?php echo site_url('save/disable-supplier/'.$value->sup_id.'/'.$value->status); ?>" class="<?php echo $value->status == '1'?'btn btn-round btn-danger btn-xs':'btn btn-round btn-success btn-xs'; ?>"><?php echo $value->status == '1'?'Disable':'Active'; ?></a>
                          
                          <a href="<?php echo site_url('admin/edit-supplier/'.$value->sup_id); ?>" class="btn btn-round btn-info btn-xs">Edit</a>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
                
               </div>
 
          </div>
          </div>
          </div>
    </div>



  </div>
</div>