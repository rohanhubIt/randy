<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>GRN</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>GRN form </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<div class="x_content">

                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo site_url('save/grn') ?>" method="post">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">GRN Date <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="grn_date"  required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">Invoice No <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="inv_no" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
                        </div>
                      </div>

                      <br><br>
                      <div class="row">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Raw Item</th>
                              <th>Qty</th>
                              <th>Buying Price</th>
                              <th>Sub total</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody id="grnd-table-body">
                            <tr>
                              <td style="width: 200px;">
                                <select class="form-control" id="id_product">
                                  <option value="">select</option>
                                  <?php foreach ($raw_mat as $key => $value): ?>
                                    <option value="<?php echo $value->raw_material_id; ?>" data-price="<?php echo $value->raw_price; ?>">
                                      <?php echo $value->raw_material_name; ?>    
                                    </option>
                                  <?php endforeach ?>
                                </select>
                              </td>
                              <td style="width: 200px;">
                                <input type="number" min="0" id="qty" name="qty" class="form-control">
                              </td>
                              <td style="width: 200px;">
                                <input type="text" id="buy_price" name="buy_price" class="form-control">
                              </td>
                              <td>
                                <input readonly="" type="text" id="sub_total" name="sub_total" class="form-control">
                              </td>                              
                              <td style="width: 200px;">
                                <button class="btn btn-primary btn-round" type="button" id="add-btn">Add</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>

                      <div class="row discription-div">

                        <div class="row">
                          <div class="col-md-4">
                            Grand Total
                          </div>
                          <div class="col-md-8">
                            <p id="gt_p">N/A</p>
                            <input class="form-control" type="hidden" id="txt_grand" name="txt_grand">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                            Discount
                          </div>
                          <div class="col-md-4">
                            <input class="form-control" type="text" id="txt_discount" name="discount">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            Net Total
                          </div>
                          <div class="col-md-4">
                            <input class="form-control" readonly="" type="text" id="txt_net_total"  name="net_total">
                          </div>
                        </div>

                      </div>


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>

</div>
  </div>
</div>

    </div>
  </div>
</div>