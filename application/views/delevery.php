
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Delevery Note</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Finish Productions form </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<div class="x_content">

                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo site_url('save/delevery') ?>" method="post">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Customer Name<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="customer_name" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Customer Mobile <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="cus_mobile"  required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Date<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="del_date"  required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <br><br>
                      <div class="row">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Product</th>
                              <th>Batch</th>
                              <th>Price</th>
                              <th>Qty</th>
                              <th>Sub Total</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody id="dele_table">
                            <tr>
                              <td>
                                <select class="form-control" id="delevery-product">
                                  <option value="">select</option>
                                  <?php foreach ($products as $key => $value): ?>

                                    <?php
                                      $CI =& get_instance();
                                      $grn_details = $CI->Master_model->getProduction($value->pro_id);
                                      $grnJson = json_encode($grn_details);
                                    ?>

                                    <option data-j='<?php echo $grnJson; ?>' value="<?php echo $value->pro_id; ?>">
                                      <?php echo $value->product_name; ?> 
                                    </option>
                                  <?php endforeach ?>
                                </select>
                              </td>
                              <td>
                                <select class="form-control" id="batch_id_delev">
                                  <option value="">select</option>
                                </select>
                              </td>
                              <td style="width: 100px;">
                                <input type="text" id="delve_prduct_price" class="form-control">
                              </td>
                              <td style="width: 100px;">
                                <input type="text" id="delev_qty" class="form-control">
                              </td>
                              <td style="width: 200px;"> 
                              	<input type="text" id="delev_sub_total" class="form-control">
                              </td>
                              <td style="width: 100px;">
                                <button id="add_delevery" class="btn btn-primary" type="button">Add</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>


                      <div class="row">
                      	<div class="col-md-4 dark-bg">
                      		<div class="row">
                      			<div class="col-md-4">
                      				<h3>Total</h3>
                      			</div>
                      			<div class="col-md-8">
                      				<h3 id="delv_total">N/A</h3>
                              <input type="hidden" id="dev_total_n" name="dev_total_n">
                      			</div>
                      		</div>
                      	</div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>

</div>
  </div>
</div>

    </div>
  </div>
</div>