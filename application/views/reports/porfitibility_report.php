<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="clearfix"></div>

    <div class="page-title">
      <div class="title_left">
        <h3>Profitibility Report</h3>
      </div>

    </div>

    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Search Options</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

                <div class="x_content">

<form method="post" action="<?php echo site_url('search-profit-report'); ?>">
                <div class="row">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date From<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="date_from" required="required" class="form-control date-pic-all col-md-7 col-xs-12" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Date To<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                      <input type="text" name="date_to" required="required" class="form-control date-pic-all col-md-7 col-xs-12" value="<?php echo date('Y-m-d'); ?>">
                    </div>
                  </div>
                </div>
                <br>
                <div class="row">
                  <div>
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>
                </div>
                <br>
</form>
                

                    <div class="row">


                        <table class="table table-bordered" id="table_issue_report">
                        <thead>
                            <tr>
                            <th>Sales Id</th>
                            <th>Date</th>
                            <th>Product Name</th>
                            <th>Qty</th>
                            <th>Cost</th>
                            <th>Total Cost</th>
                            <th>Price</th>
                            <th>Total Price</th>
                            <th>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php 
                            $row = 1;  
                            $table_total_price = 0;
                            $table_total_cost = 0;

                            $all_total_sales = 0;
                            $all_total_cost = 0;
                            $all_total_profit = 0;
                          ?>
                          <?php foreach($profitReport as $key => $value): ?>
                            <tr>
                              <td>
                                <?php 
                                  $code = str_pad($value->sale_note_id, 4, '0', STR_PAD_LEFT);
                                  echo  "SN_".$code;
                                ?>
                              </td>
                              <td>
                                <?php echo $value->sd_date ?>
                              </td>
                              <td>
                                
                                <?php echo $value->pro_code." - ".$value->guage." - ".$value->pro_name." - ".$value->Description; ?>

                              </td>
                              <td>
                                <?php echo $value->qty ?>
                              </td>
                              <td>
                                <?php echo $value->cost_price ?>
                              </td>
                              <td>
                                <?php 
                                  $total_cost = $value->cost_price * $value->qty;
                                  echo $total_cost; 
                                  $table_total_cost += $total_cost;
                                ?>
                              </td> 
                              <td>
                                <?php echo $value->price ?>
                              </td>   
                              <td>
                                <?php 
                                  $total_price = $value->price *  $value->qty;
                                  echo $total_price; 
                                  $table_total_price += $total_price;
                                ?>
                              </td>  
                              <td>
                                <?php 
                                  $profit = $total_price - $total_cost;  
                                  echo $profit;


                                ?>
                              </td>
                            </tr>

                            <?php
                              $CI =& get_instance();
                              $s_n_details = $CI->Reports_model->selectSaleDetails($value->sale_note_id);
                              $row_details = $CI->Reports_model->selectSaleDetailRowCount($value->sale_note_id);
                            ?>

                          <?php if($row_details == $row): ?>
                            <tr style="background: #d9d4d4;">
                              <td>
                                Selling Total
                              </td>
                              <td>
                                <?php 
                                  echo $table_total_price; 
                                  $all_total_sales += $table_total_price;
                                ?>
                              </td>
                              <td>
                                Cost Total
                              </td>
                              <td>
                                <?php 
                                  echo $table_total_cost; 
                                  $all_total_cost += $table_total_cost;
                                ?>
                              </td>
                              <td>
                                Discount
                              </td>
                              <td>
                                <?php 
                                  $discount = $s_n_details[0]->discount; 
                                  echo $discount;
                                ?>
                              </td>
                              <td>
                                Profit
                              </td>
                              <td>
                                <?php  
                                  $total_prf = $table_total_price - $table_total_cost - $discount; 
                                  echo $total_prf;
                                  $all_total_profit += $total_prf; 
                                ?>
                              </td>
                              <td></td>
                            </tr>
                            <?php $row = 1; ?>
                          <?php endif ?>
                          <?php $row++; ?>
                          <?php endforeach ?>
                        </tbody>
                        </table>
                    </div>
                </div>

                </div>
        </div>
        </div>
        </div>

        <div class="row">
          <div class="col-md-12">
          <table class="table table-bordered">
            <tr>
              <td>Total Selling</td>
              <td>
                <?php echo $all_total_sales; ?>
              </td>
            </tr>
            <tr>
              <td>Total Cost</td>
              <td>
                <?php echo $all_total_cost; ?>
              </td>
            </tr>
            <tr>
              <td>Total Profit</td>
              <td><?php echo $all_total_profit; ?></td>
            </tr>
          </table>
          </div>
        </div>

  </div>
</div>