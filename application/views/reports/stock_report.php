<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="clearfix"></div>

    <div class="page-title">
      <div class="title_left">
        <h3>Stock Report</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

<div class="clearfix"></div>


    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Stock Table</h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
                 <table class="table table-bordered common_grid" id="table_issue_report">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Product Code</th>
                      <th>Product</th>
                      <th>Qty</th>
                      <th>Location</th>
                      <th>Stock Type</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    <?php foreach ($stock as $key => $value): ?>
                    	<tr>
                    		<td><?php echo $value->stock_id ?></td>
                    		<td><?php echo $value->pro_code ?></td>
                    		<td>
                          <?php echo $value->guage." - ".$value->pro_name." - ".$value->Description; ?>
                        </td>
                    		<td>
                    			
                    			<?php
                    			      $CI =& get_instance();
				                      $dat = $CI->Reports_model->qtySUM($value->product_id);
				                      echo $dat[0]->st_qty;
                    			?>

                    		</td>
                    		<td><?php echo ($value->location == "2")? "Moving": "Ground"; ?></td>
                    		<td><?php echo ($value->st_type == "2")? "Sell": "Issue"; ?></td>
                    	</tr>
                    <?php endforeach ?>

                  </tbody>
                </table>                


               </div>
 
          </div>
          </div>
          </div>
    </div>



</div>
</div>