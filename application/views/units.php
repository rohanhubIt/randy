<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Units</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Add Units</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<div class="x_content">

<form action="<?php echo site_url('save/save-unit') ?>" method="post" class="form-horizontal form-label-left">
      
      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Unit Name <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input type="text" name="unit_name" required="required" class="form-control col-md-7 col-xs-12">
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Status <span class="required">*</span>
        </label>
        <div class="col-md-6 col-sm-6 col-xs-12">
        	<select class="form-control" name="status">
        		<option value="1">Active</option>
        		<option value="2">Deactive</option>
        	</select>
        </div>
      </div>

      <div class="ln_solid"></div>
      <div class="form-group">
        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
          <button class="btn btn-primary" type="button">Cancel</button>
		  <button class="btn btn-primary" type="reset">Reset</button>
          <button type="submit" class="btn btn-success">Submit</button>
        </div>
      </div>

</form>


</div>

				</div>
			</div>
		</div>



    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Unit table </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
                <table class="table table-bordered common_grid">
                  <thead>
                    <tr>
                      <th>Unit Id</th>
                      <th>Name</th>
                      <th>Status</th>
                      <th>Acions</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php foreach ($table_data as $key => $value): ?>
                    <tr>
                      <td><?php echo $value->unit_id; ?></td>
                      <td><?php echo $value->unit_name; ?></td>
                      <td><?php echo ($value->status == "1")? "Active": "Deactive"; ?></td>
                      <td></td>
                    </tr>
                  <?php endforeach ?>
                  </tbody>
                </table>
                
               </div>
 
          </div>
          </div>
          </div>
    </div>


  </div>
</div>