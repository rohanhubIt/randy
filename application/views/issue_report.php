<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="clearfix"></div>

    <div class="page-title">
      <div class="title_left">
        <h3>Issue Note Report</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

<div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Search Options</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<div class="x_content">
	<div class="row">
		<div class="col-md-4 form-group">
        	<label class="control-label col-md-12 col-sm-12 col-xs-12" for="first-name">Note No</label>
			<input type="text" name="code" id="code" class="form-control" placeholder="">
		</div>

		<div class="col-md-4 form-group">
        	<label class="control-label col-md-12 col-sm-12 col-xs-12" for="first-name">Date From</label>
			<input type="text" name="from_date" id="from-date" class="form-control date-pic-all" placeholder="">
		</div>

		<div class="col-md-4 form-group">
        	<label class="control-label col-md-4 col-sm-4 col-xs-12" for="first-name">Date to</label>
			<input type="text" name="to_date" id="to-date" class="form-control date-pic-all" placeholder="">
		</div>

	</div>
</div>
					</div>
				</div>
			</div>



    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Issue Note Table</h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
 
                 <table class="table table-bordered" id="table_issue_report">
                  <thead>
                    <tr>
                      <th>Note No</th>
                      <th>Remark</th>
                      <th>Date</th>
                      <th>From</th>
                      <th>To</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($table_data as $key => $value): ?>
                      <tr>
                        <td>
                          <?php 
                            $code = str_pad($value->issue_note_id, 4, '0', STR_PAD_LEFT);
                            echo  "INT_".$code;
                          ?>
                        </td>
                        <td>
                          <?php echo ($value->remark == "")? "N/A": $value->remark;  ?>
                        </td>
                        <td>
                          <?php echo ($value->issue_date == "")? "N/A": $value->issue_date;  ?>
                        </td>
                        <td>
                          <?php echo ($value->from_stock == "1")? "Ground": "Moving";  ?>
                        </td>
                        <td>
                          <?php echo ($value->to_stock == "1")? "Ground": "Moving";  ?>
                        </td>
                        <td>

                          <a href="<?php echo site_url('view-issue-note/').$value->issue_note_id; ?>" class="btn btn-info btn-xs">
                          View
                          </a>
                          
                        </td>                                            
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>



               </div>
          </div>
          </div>
          </div>
    </div>


    </div>
  </div>

  <script type="text/javascript">
    var table_issue_report = $('#table_issue_report').DataTable();

    $.fn.dataTable.ext.search.push(
        function( settings, data, dataIndex ) {

            var code = data[0]; // use data for the age column
            var date = data[2];

            var in_code = $("#code").val();
            var in_from_date = $("#from-date").val();
            var in_to_date = $("#to-date").val();

            // search seq 1
            if(in_code != "" && in_from_date != "" && in_to_date != "") {
                if(in_code == code && in_from_date <= date && in_to_date >= date) {
                    return true;
                } else {
                    return false;
                }
            }

            // search seq 2
            if(in_from_date != "" && in_to_date != "") {
                if(in_from_date <= date && in_to_date >= date) {
                    return true;
                } else {
                    return false;
                }                
            }

            if(in_code != "") {
                if(in_code == code) {
                    return true;
                } else {
                    return false;
                }                
            } else {
                return true;
            }



        }
    );

    $("#code").on("keyup", function () {
        table_issue_report.draw();
    });

    $("#to-date").on("change", function() {
        if($("#from-date").val() == "") {
            alert("Please fill from date");
        }
        table_issue_report.draw(); 
    });
</script>