<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Production</h3>
      </div>

	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search for...">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go!</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Production Form </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                      </li>
                      
                    </ul>
                    <div class="clearfix"></div>
                  </div>

<div class="x_content">

                    <br />
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="<?php echo site_url('save/production') ?>" method="post">

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Product<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="product">
                          	<option value="">select</option>
                            <?php foreach ($products as $key => $value): ?>
                              <option value="<?php echo $value->pro_id; ?>"><?php echo $value->product_name; ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Production Date <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="production_date"  required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="middle-name" class="control-label col-md-3 col-sm-3 col-xs-12">End Qty <span class="required">*</span></label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input name="end_qty" id="end_qty" class="form-control col-md-7 col-xs-12" type="text" name="middle-name">
                        </div>
                      </div>
                      <br><br>
                      <div class="row">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th>Raw Item</th>
                              <th>Batch</th>
                              <th>Price</th>
                              <th>Qty</th>
                              <th>Sub Total</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody id="pro_table_body">
                            <tr>
                              <td style="width: 200px;">
                                <select class="form-control" id="raw_item">
                                  <option value="">select</option>
                                  <?php foreach ($raw_mat as $key => $value): ?>

                                    <?php
                                      $CI =& get_instance();
                                      $grn_details = $CI->Master_model->getGRN($value->raw_material_id);

                                      $grnJson = json_encode($grn_details);
                                    ?>

                                    <option value="<?php echo $value->raw_material_id; ?>" data-grn='<?php echo $grnJson; ?>'>
                                      <?php echo $value->raw_material_name; ?>
                                    </option>
                                  <?php endforeach ?>
                                </select>
                              </td>
                              <td style="width: 200px;">
                                <select class="form-control" id="batch_id">
                                  <option value="">select</option>
                                </select>
                              </td>
                              <td >
                                <input type="text" readonly="" id="price" name="price" class="form-control">
                              </td>
                              <td >
                                <input type="hidden" name="raw_qty" id="raw_qty">
                                <input type="text" name="qty" id="pro_qty" class="form-control">
                              </td>
                              <td  style="width: 200px;">
                                <input type="text" readonly="" id="sub_total" name="sub_total" class="form-control">
                              </td>

                              <td>
                                <button id="pro-add-btn" class="btn btn-primary" type="button">Add</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>



                      <div class="row discription-div">

                        <div class="row">
                          <div class="col-md-4">
                            Grand Total
                          </div>
                          <div class="col-md-8">
                            <p id="pro_gt_p">N/A</p>
                            <input class="form-control" type="hidden" id="txt_pro_grand" name="txt_pro_grand">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                            Product Cost
                          </div>
                          <div class="col-md-4">
                             <p id="production_cost">N/A</p>
                             <input type="hidden" name="pro_product_cost" id="pro_product_cost">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                            Product Tax
                          </div>
                          <div class="col-md-4">
                            <input class="form-control" type="text" id="txt_pro_product_tax"  name="txt_pro_product_tax">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-md-4">
                            Product Value
                          </div>
                          <div class="col-md-4">
                            <p id="pro_price_p">N/A</p>
                            <input type="hidden" name="pro_product_value" id="pro_product_value">
                          </div>
                        </div>

                      </div>


                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>

</div>
  </div>
</div>

    </div>
  </div>
</div>