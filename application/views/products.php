<!-- page content -->

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Products

          <a href="<?php echo site_url('admin/import-csv') ?>" class="btn btn-info" onclick="window.print()">
            <i class="fa fa-download" aria-hidden="true"></i>
            Import CSV
          </a>

        </h3>
      </div>


  	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search....">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>


	<div class="clearfix"></div>

              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Products form </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
	             <div class="x_content">
        
                <br/>

<?php if ($this->uri->segment(3) == ""): ?>
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo site_url('save/product') ?>">
<?php else: ?>
    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo site_url('edit/product') ?>">  

    <input type="hidden" name="pro_id" value="<?php echo $this->uri->segment(3) ?>">

<?php endif ?>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Product Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="prodcut-name" name="product_name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($products == "")? "": $products[0]->pro_name ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Product Type<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="type">
                            <option value="">Select</option>
                            <?php foreach ($categories as $key => $value): ?>
                              
                              <?php if ($products != null): ?>
                               
                               <option <?php echo ($value->cat_id == $products[0]->pro_type)? "selected":"" ?> value="<?php echo $value->cat_id ?>">
                                <?php echo $value->categories ?>
                               </option>

                              <?php else: ?>

                               <option value="<?php echo $value->cat_id ?>">
                                <?php echo $value->categories ?>
                               </option>

                              <?php endif ?>



                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Brand name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="rawmaterial-name" name="brand_name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($products == "")? "": $products[0]->pro_name ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Guage <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="rawmaterial-name" name="guage" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($products == "")? "": $products[0]->guage ?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Unit Type <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select class="form-control" name="unit_type">
                            <option value="">select</option>
                            <?php foreach ($unit_type as $key => $value): ?>

                              <?php if ($products != null): ?>

                                <option <?php echo ($value->unit_id == $products[0]->unit_type)? "selected": "" ?> value="<?php echo $value->unit_id ?>">
                                  <?php echo $value->unit_name ?>
                                </option>

                              <?php else: ?>
                                <option value="<?php echo $value->unit_id ?>">
                                  <?php echo $value->unit_name ?>
                                </option>
                              <?php endif; ?>
                            <?php endforeach ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Unit
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="rawmaterial-name" name="unit" class="form-control col-md-7 col-xs-12"

                          value="<?php echo ($products == "")? "": $products[0]->unit ?>"

                          >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Cost 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="cost" name="cost" class="form-control col-md-7 col-xs-12"
                          value="<?php echo ($products == "")? "": $products[0]->cost ?>"
                          >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Selling Price 
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="price" name="price" class="form-control col-md-7 col-xs-12"
                          value="<?php echo ($products == "")? "": $products[0]->price ?>"
                          >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Description <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <textarea class="form-control" name="description" value="<?php echo ($products == "")? "": $products[0]->Description ?>">
                            
                            <?php echo ($products == "")? "": $products[0]->Description ?>

                          </textarea>
                        </div>
                      </div>

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>

 
          </div>
          </div>
          </div>
    </div>


    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Products table </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
                <table class="table table-bordered common_grid">
                  <thead>
                    <tr>
                      <th>Product Id</th>
                      <th>Product Code</th>
                      <th>Product Name</th>
                      <th>Brand</th>
                      <th>Guage</th>
                      <th>Description</th>
                      <th>Cost</th>
                      <th>Sel.. Price</th>
                      <th>Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($table_data as $key => $value): ?>
                      <tr>
                        <td><?php echo $value->pro_id ?></td>
                        <td><?php echo $value->pro_code ?></td>
                        <td><?php echo $value->pro_name ?></td>
                        <td><?php echo $value->brand_name ?></td>
                        <td><?php echo $value->guage ?></td>
                        <td><?php echo $value->Description ?></td>
                        <td><?php echo $value->cost ?></td>
                        <td><?php echo $value->price ?></td>
                        <td><?php echo ($value->status == "1")? "Active": "Deactive"; ?></td>
                        <td>
                          <a href="" class="btn btn-xs btn-info">View More</a>
                          <a href="<?php echo site_url('admin/product/').$value->pro_id ?>" class="btn btn-xs btn-success">Edit</a>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
                
               </div>
 
          </div>
          </div>
          </div>
    </div>


  </div>
</div>




    </div>
  </div>
</div>