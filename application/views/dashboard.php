<?php if($this->session->has_userdata('client_auth')): ?>  
  <?php
    $userdata = $this->session->userdata('client_auth');
    $userdata = json_decode($userdata);
  ?>
<?php endif; ?>
<!-- page content -->
<div class="right_col" role="main">
  <div class="">
	<div class="clearfix"></div>

	<div class="row">

<?php if ($userdata[0]->type == "1"): ?>	  
	  <div class="col-xs-6 col-md-3">
	    <a href="<?php echo site_url() ?>admin/product" class="thumbnail">
	      <i class="fa fa-archive" aria-hidden="true"></i>
	      <h4>Products</h4>
	    </a>
	  </div>

	  <div class="col-xs-6 col-md-3">
	    <a href="<?php echo site_url() ?>admin/customer" class="thumbnail">
	      <i  class="fa fa-users" aria-hidden="true"></i>
	      <h4>Customers</h4>

	    </a>
	  </div>

	  <div class="col-xs-6 col-md-3">
	    <a href="<?php echo site_url() ?>admin/units" class="thumbnail">
	      <i class="fa fa-balance-scale" aria-hidden="true"></i>
	      <h4>Units</h4>
	    </a>
	  </div>

	  <div class="col-xs-6 col-md-3">
	    <a href="<?php echo site_url() ?>admin/categories" class="thumbnail">
	      <i class="fa fa-sitemap" aria-hidden="true"></i>
	      <h4>Categories</h4>
	    </a>
	  </div>

<!-- sec line -->

	  <div class="col-xs-6 col-md-3">
	    <a href="<?php echo site_url('admin/issue-note') ?>" class="thumbnail">
	   	  <i class="fa fa-files-o" aria-hidden="true"></i>
	      <h4>Issue Note</h4>
	    </a>
	  </div>
<?php endif; ?>

	  <div class="col-xs-6 col-md-3">
	    <a href="<?php echo site_url('admin/sales-note') ?>" class="thumbnail">
	      <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
	      <h4>Sales Note</h4>
	    </a>
	  </div>

<?php if ($userdata[0]->type == "1"): ?>
	  <div class="col-xs-6 col-md-3">
	    <a href="#" class="thumbnail">
	    	<i class="fa fa-file-text-o" aria-hidden="true"></i>
	      <h4>Report 1</h4>
	    </a>
	  </div>

	  <div class="col-xs-6 col-md-3">
	    <a href="#" class="thumbnail">
	    	<i class="fa fa-file-text-o" aria-hidden="true"></i>
	      <h4>Report 2</h4>
	    </a>
	  </div>
<?php endif; ?>

	</div>

    </div>
  </div>
</div>