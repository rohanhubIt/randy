<!-- page content -->

<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Raw Material</h3>
      </div>


  	  <div class="title_right">
	    <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
	      <div class="input-group">
	        <input type="text" class="form-control" placeholder="Search....">
	        <span class="input-group-btn">
	          <button class="btn btn-default" type="button">Go</button>
	        </span>
	      </div>
	    </div>
	  </div>
	</div>


	<div class="clearfix"></div>

              <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Raw Material form </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
	             <div class="x_content">
        
                <br/>

                  <?php if($form_data== null): ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo site_url('save/rawmaterial') ?>">
                  <?php else: ?>
                    <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo site_url('save/update-rawmaterial/'.$form_data[0]->raw_material_id) ?>">
                  <?php endif; ?>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="rawmaterial-name">Raw Material Name <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="rawmaterial-name" name="rawmaterial_name" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($form_data != null)? $form_data[0]->raw_material_name: '';?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Raw Material Qty<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="Qty" name="qty" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($form_data != null)? $form_data[0]->raw_material_quantity: '';?>">
                        </div>
                      </div>


                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="last-name">Raw Material Price<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" id="price" name="price" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo ($form_data != null)? $form_data[0]->raw_price: '';?>">
                        </div>
                      </div>

                     <!--  <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Gender</label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <div id="gender" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="gender" value="1"> &nbsp; Male &nbsp;
                            </label>
                            <label class="btn btn-primary" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                              <input type="radio" name="gender"  value="0"> Female
                            </label>
                          </div>
                        </div>
                      </div> -->

                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                          <button class="btn btn-primary" type="button">Cancel</button>
						  <button class="btn btn-primary" type="reset">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>

 
          </div>
          </div>
          </div>
    </div>



    <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                <div class="x_title"> 
                <h2>Raw Material table </h2>
                <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
               <div class="x_content">
                <br><br>
                <table class="table table-bordered common_grid">
                  <thead>
                    <tr>
                      <th>Raw Material Id</th>
                      <th>Raw Material Name</th>
                      <th>Raw Material Qty</th>
                      <th>Raw Material Status</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($table_data as $key => $value): ?>
                      <tr>
                        <td><?php echo $value->raw_material_id; ?></td>
                        <td><?php echo $value->raw_material_name; ?></td>
                        <td><?php echo $value->raw_material_quantity; ?></td>
                        <td><?php echo ($value->is_active == "1")? "Active":"Deactive"; ?></td>
                        <td>
                          <a href="<?php echo site_url('save/disable-raw-material/'.$value->raw_material_id.'/'.$value->is_active); ?>" class="<?php echo $value->is_active == '1'?'btn btn-round btn-danger btn-xs':'btn btn-round btn-success btn-xs'; ?>"><?php echo $value->is_active == '1'?'Disable':'Active'; ?></a>
                          
                          <a href="<?php echo site_url('admin/edit-raw-material/'.$value->raw_material_id); ?>" class="btn btn-round btn-info btn-xs">Edit</a>
                        </td>
                      </tr>
                    <?php endforeach ?>
                  </tbody>
                </table>
                
               </div>
 
          </div>
          </div>
          </div>
    </div>



  </div>
</div>




    </div>
  </div>
</div>