<?php
class Reports_model extends CI_Model {

	function selectStock() {
		$this->db->select('*');
		$this->db->from('stock');

		$this->db->join('products', 'products.pro_id = stock.product_id');

		$this->db->group_by("stock.product_id");

		$sql = $this->db->get();

		$result = $sql->result();
		return  $result;
	}

	function qtySUM($product_id) {
		$this->db->select('SUM(qty) AS st_qty');
		$this->db->where('product_id', $product_id);
		$sql = $this->db->get('stock');
		$result = $sql->result();
		return $result;
	}

	function selectProfitReport($date_in = null, $data_out = null) {
		$this->db->select('*');
		$this->db->from('sale_note');
		$this->db->join('sale_details', 'sale_details.sale_id = sale_note.sale_note_id');
		$this->db->join('products', 'products.pro_id = sale_details.pro_id');

		if($date_in != null) {
			$this->db->where('sale_note.sd_date >=', $date_in);
			$this->db->where('sale_note.sd_date <=', $data_out);
		} else {
			$this->db->limit(100);
		}
		
		$sql = $this->db->get();
		$result = $sql->result();
		return $result;
	}

	function selectSaleDetails($sale_id) {
		$this->db->select('*');
		$this->db->where('sale_note_id', $sale_id);
		$sql = $this->db->get('sale_note');
		$result = $sql->result();
		return $result;
	}

	function selectSaleDetailRowCount($sale_id) {
		$this->db->select('*');
		$this->db->where('sale_id', $sale_id);
		$sql = $this->db->get('sale_details');
		$result = $sql->num_rows();
		return $result;
	}

	function selectAvaliableStock($pro_id) {
		$this->db->select('SUM(qty) as qty');
		$this->db->where('product_id', $pro_id);
		$sql = $this->db->get('stock');
		$result = $sql->result();
		return $result;
	}
}