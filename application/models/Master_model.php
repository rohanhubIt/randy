<?php

class Master_model extends CI_Model {

	function common_insert($table, $data) {
		$sql = $this->db->insert($table, $data);
		return $sql;
	}

	function common_select($table, $where= null, $id= null) {
		$this->db->select('*');
		if($where != null){
			$this->db->where($where, $id);
		}
		$sql = $this->db->get($table);
		$result = $sql->result();
		return $result;
	}

	function updateMaster($data, $table, $where, $value) {
      $sql = $this->db->where($where, $value)->update($table, $data);
      if($sql) {
          return true;
      }
      return false;
    }

	function loginMethod($user, $pass) {
		$this->db->where('username', $user);
		$this->db->where('password', $pass);
		$sql = $this->db->get('user');
		$result = $sql->result();
		return $result;
	}

	function getGRN($id=null) {
		
		$this->db->select('*');
		$this->db->from('grn_detail');
		$this->db->join('grn_items', 'grn_items.grn_detail_id = grn_detail.grn_id');

		if($id != null) {
			$this->db->where('grn_items.pro_id', $id);
		}

		$sql = $this->db->get();
		$result = $sql->result();
		return $result;

	}

	function getProduction($id=null) {
		$this->db->select('*');
		$this->db->from('product');
		$this->db->join('production', 'production.product_id = product.pro_id');

		if($id != null) {
			$this->db->where('product.pro_id', $id);
		}

		$sql = $this->db->get();
		$result = $sql->result();
		return $result;		
	}

	function getlastproduct() {
		$this->db->select_max('pro_id');
		$sql = $this->db->get('products');
		$result = $sql->result();
		return $result;			
	}

	function issueNote($data, $data_detail, $data_stocks) {
		$this->db->trans_begin();


		// run queries here
		
		$this->db->insert_batch('issue_note_detail', $data_detail);
		$this->db->insert_batch('stock', $data_stocks);


		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		    return false;
		}

	    $this->db->trans_commit();
	    return true;
			
	}

	function salesNote($data, $data_detail, $data_stocks) {
		$this->db->trans_begin();


		// run queries here
		
		$this->db->insert_batch('sale_details', $data_detail);
		$this->db->insert_batch('stock', $data_stocks);


		if ($this->db->trans_status() === FALSE){
		    $this->db->trans_rollback();
		    return false;
		}

	    $this->db->trans_commit();
	    return true;		
	}

	function getSales() {
		$this->db->select('*');
		$this->db->from('sale_note');

		$this->db->join('customer', 'customer.cus_id = sale_note.customer');


		$sql = $this->db->get();
		$result = $sql->result();
		return $result;		
	}

	function getIssueNoteDetail($segOne) {
		$this->db->select('*');
		$this->db->from('issue_note_detail');

		$this->db->join('products', 'products.pro_id = issue_note_detail.pro_id');
		$this->db->where('issue_note_detail.issue_note_id', $segOne);

		$sql = $this->db->get();
		$result = $sql->result();
		return $result;			
	}

	function getSalesNote($segOne) {
		$this->db->select('*');
		$this->db->from('sale_note');

		$this->db->join('customer', 'customer.cus_id = sale_note.customer');
		$this->db->where('sale_note.sale_note_id', $segOne);

		$sql = $this->db->get();
		$result = $sql->result();
		return $result;	

	}

	function getSalesNoteDetail($segOne) {
		$this->db->select('*');
		$this->db->from('sale_details');

		$this->db->join('products', 'products.pro_id = sale_details.pro_id');
		$this->db->where('sale_details.sale_id', $segOne);
		
		$sql = $this->db->get();
		$result = $sql->result();
		return $result;		
	}
}