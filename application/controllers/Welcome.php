<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Colombo");

        if ($this->session->userdata('client_auth'))
        {
            $user_data = $this->session->userdata('client_auth');
            $user_data = (string) $user_data;
            $this->userdata = json_decode($user_data);
        } else {
           redirect('login');
        }
    }

	function uiDashboard() {

		$data = (object) NULL;

		$this->load->view('common/header');
		$this->load->view('dashboard', $data);
		$this->load->view('common/footer');
	}

	function uiRawMaterial() {

		$data = (object)NULL;
		$data->form_data= null;
		$data->table_data = $this->Master_model->common_select('raw_material');

		$segOne = $this->uri->segment(3);

		if($segOne){
			$data->form_data = $this->Master_model->common_select('raw_material', 'raw_material_id', $segOne);
		}
		$this->load->view('common/header');
		$this->load->view('rawmaterial', $data);
		$this->load->view('common/footer');
	}
	function uiSupplier(){

		$data = (object)NULL;
		$data->form_data= null;
		$data->table_data = $this->Master_model->common_select('supplier');

		$segOne = $this->uri->segment(3);
		if($segOne){
			$data->form_data = $this->Master_model->common_select('supplier', 'sup_id', $segOne);
		}

		$this->load->view('common/header');
		$this->load->view('supplier', $data);
		$this->load->view('common/footer');
	}

	function uiCustomer() {

		$data = (object)NULL;
		$data->form_data= null;
		$data->table_data = $this->Master_model->common_select('customer');

		$segOne = $this->uri->segment(3);
		if($segOne){
			$data->form_data = $this->Master_model->common_select('customer', 'cus_id', $segOne);
		}

		$this->load->view('common/header');
		$this->load->view('customer', $data);
		$this->load->view('common/footer');
	}

	function uiProduction() {

		$data = (object)NULL;
		$data->products = $this->Master_model->common_select('product');
		$data->raw_mat = $this->Master_model->common_select('raw_material');

		$this->load->view('common/header');
		$this->load->view('production', $data);
		$this->load->view('common/footer');
	}

	function uiDesign() {
		$this->load->view('common/header');
		$this->load->view('design');
		$this->load->view('common/footer');
	}

	function uiSalesPos() {
	   	$this->load->view('common/header');
		$this->load->view('salespos');
		$this->load->view('common/footer');	
	}

	function uiSalesCredit() {
		$this->load->view('common/header');
		$this->load->view('salescredit');
		$this->load->view('common/footer');	
	}

	function uiCancelledInvoices() {
		$this->load->view('common/header');
		$this->load->view('uiCancelledInvoices');
		$this->load->view('common/footer');	
	}

	function uiProducts() {

		$data = (object)NULL;

		$data->categories = $this->Master_model->common_select('categories', 'status', '1');
		$data->unit_type = $this->Master_model->common_select('unit', 'status', '1');

		$segOne = $this->uri->segment(3);

		$data->products = "";
		if($segOne != null) {
			$data->products = $this->Master_model->common_select('products', 'pro_id', $segOne);
		}

		$data->table_data = $this->Master_model->common_select('products');

		$this->load->view('common/header');
		$this->load->view('products', $data);
		$this->load->view('common/footer');			
	}

	function uiGrn() {

		$data = (object)NULL;
		$data->raw_mat = $this->Master_model->common_select('raw_material');

		$this->load->view('common/header');
		$this->load->view('grn', $data);
		$this->load->view('common/footer');		
	}

	function uiCompleteItems() {
		$this->load->view('common/header');
		$this->load->view('complete_items');
		$this->load->view('common/footer');			
	}

	function uiDeliveryNote() {
		
		$data = (object)NULL;
		$data->products = $this->Master_model->common_select('product');

		$this->load->view('common/header');
		$this->load->view('delevery', $data);
		$this->load->view('common/footer');			
	}

	function uiReturnNote() {
		$this->load->view('common/header');
		$this->load->view('return_note');
		$this->load->view('common/footer');			
	}

	function uiEquipment() {

		$data = (object)NULL;
		$data->table_data = $this->Master_model->common_select('equipment');		
		$data->form_data= null;

		$segOne = $this->uri->segment(3);
		if($segOne){
			$data->form_data = $this->Master_model->common_select('equipment', 'eq_id', $segOne);
		}

		$this->load->view('common/header');
		$this->load->view('equipment', $data);
		$this->load->view('common/footer');			
	}

	function userRegistration() {

		$data = (object)NULL;
		$data->user_types = $this->Master_model->common_select('user_type');		
		$data->table_data = $this->Master_model->common_select('user');		

		$this->load->view('common/header');
		$this->load->view('user', $data);
		$this->load->view('common/footer');	
				
	}

	function uiCategories() {
		$data = (object)NULL;

		$data->table_data = $this->Master_model->common_select('categories');

		$this->load->view('common/header');
		$this->load->view('category', $data);
		$this->load->view('common/footer');

	}

	function uiUnits() {
		$data = (object)NULL;

		$data->table_data = $this->Master_model->common_select('unit');

		$this->load->view('common/header');
		$this->load->view('units', $data);
		$this->load->view('common/footer');		
	}

	function issueNot() {
		$data = (object)NULL;

		$data->product = $this->Master_model->common_select('products', 'status', '1');
		$data->table_data = $this->Master_model->common_select('issue_note');

		$this->load->view('common/header');
		$this->load->view('issue_note', $data);
		$this->load->view('common/footer');		
	}

	function salesNote() {
		$data = (object)NULL;

		$data->product = $this->Master_model->common_select('products', 'status', '1');
		$data->cusomers = $this->Master_model->common_select('customer', 'status', '1');

		$data->table_data = $this->Master_model->getSales();

		$this->load->view('common/header');
		$this->load->view('sales_note', $data);
		$this->load->view('common/footer');			
	}

	function viewIssueNote() {
		$data = (object)NULL;

		$segOne = $this->uri->segment(2);
		$data->issue_note = $this->Master_model->common_select('issue_note', 'issue_note_id', $segOne);
		$data->issue_noteDetail = $this->Master_model->getIssueNoteDetail($segOne);


		$this->load->view('common/header');
		$this->load->view('issue_note_view', $data);
		$this->load->view('common/footer');	

	}

	function viewSalesNote() {
		$data = (object)NULL;

		$segOne = $this->uri->segment(2);
		$data->sales_note = $this->Master_model->getSalesNote($segOne);
		$data->sales_noteDetail = $this->Master_model->getSalesNoteDetail($segOne);


		$this->load->view('common/header');
		$this->load->view('sales_note_view', $data);
		$this->load->view('common/footer');			
	}

	function importCSV() {
		$data = (object)NULL;

		$this->load->view('common/header');
		$this->load->view('import_csv', $data);
		$this->load->view('common/footer');			
	}

	function logout() {
		$this->session->sess_destroy();
        redirect(base_url());
	}

}
