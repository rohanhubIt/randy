<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {


	public function __construct() {
	    parent::__construct();
		$this->load->model('Sales_model');
	}

 function savePoscash() {
 	$data = array(
				'date' => $this->input->post('date'),
				// 'product_name' => $this->input->post('product_name'),
				// 'quantity'=> $this->input->post('quantity'),
				// 'unit_quantity' => $this->input->post('unit_quantity'),
				'status'=> '1'
			);

		$sql = $this->Sales_model->common_insert('sale', $data);

		if($sql) {

		} else {

		}
		redirect($_SERVER['HTTP_REFERER']);
 }

 function Delevery() {
 	$data = array(
 				'cust_name' => $this->input->post('customer_name'),
 				'mobile' => $this->input->post('cus_mobile'),
 				'total' => $this->input->post('dev_total_n'),
 				'dev_date' => $this->input->post('del_date'),
			);

		$sql = $this->Master_model->common_insert('delevery_note', $data);

		if($sql) {
			$last = $this->db->insert_id();

			$pro_id = $this->input->post('dev_product');
			$qty = $this->input->post('dev_qty');
			$sel_price = $this->input->post('dev_sel_price');
			$batch = $this->input->post('dev_batch');

			$ins_arr = array();
			foreach ($pro_id as $key => $value) {
				array_push($ins_arr, array('del_id' => $last,
				'pro_id' =>  $value,
				'batch' => $batch[$key],
				'qty' => $qty[$key],
				'price' => $sel_price[$key]));

				$proMet = $this->Master_model->common_select('production', 'production_id', $batch[$key]);

				$crrQty = $proMet[0]->endQty;

				$newQty = $crrQty - $qty[$key];

				$this->db->where('production_id', $batch[$key]);
				$this->db->update('production', array('endQty'=>$newQty));

			}

			$this->db->insert_batch('delevery_detail', $ins_arr);



		} else {

		}
		redirect($_SERVER['HTTP_REFERER']); 	
 }

} 