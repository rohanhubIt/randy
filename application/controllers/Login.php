<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index()
	{
		$this->load->view('welcome_message');
	}

	function loginAction() {
		$userName = $this->input->post('username');
		$pass = md5($this->input->post('password'));

		$user = $this->Master_model->loginMethod($userName, $pass);
		if(count($user) > 0) {
			$user_data = json_encode($user);
			$this->session->set_userdata('client_auth', $user_data);
			
	        redirect('admin/dashboard', 'refresh');
		} else {
			redirect('/', 'refresh');
		}

	}
}