<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set("Asia/Colombo");

        if ($this->session->userdata('client_auth'))
        {
            $user_data = $this->session->userdata('client_auth');
            $user_data = (string) $user_data;
            $this->userdata = json_decode($user_data);
        } else {
           redirect('login');
        }
    }

	function issueReport() {
		$data = (object) NULL;

		$data->table_data = $this->Master_model->common_select('issue_note');

		$this->load->view('common/header');
		$this->load->view('reports/issue_report', $data);
		$this->load->view('common/footer');
	}

	function salesReport() {
		$data = (object) NULL;

		$data->table_data = $this->Master_model->getSales();

		$this->load->view('common/header');
		$this->load->view('reports/sale_report', $data);
		$this->load->view('common/footer');
	}

	function stockReport() {
		$data = (object) NULL;

		$data->stock = $this->Reports_model->selectStock();

		$this->load->view('common/header');
		$this->load->view('reports/stock_report', $data);
		$this->load->view('common/footer');
	}

	function profitibilityReport() {
		$data = (object) NULL;

		$data->profitReport = $this->Reports_model->selectProfitReport();

		$this->load->view('common/header');
		$this->load->view('reports/porfitibility_report', $data);
		$this->load->view('common/footer');		
	}

	function profitibilityReportSearch() {

		$data = (object) NULL;

		$date_from = $this->input->post('date_from');
		$date_to = $this->input->post('date_to');

		$data->profitReport = $this->Reports_model->selectProfitReport($date_from, $date_to);

		$this->load->view('common/header');
		$this->load->view('reports/porfitibility_report', $data);
		$this->load->view('common/footer');	
		
	}
}