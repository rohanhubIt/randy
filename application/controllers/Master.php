<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {
	function saveCustomer() {
		$data = array(
				'first_name' => $this->input->post('f_name'),
				'last_name' => $this->input->post('l_name'),
				'address'=> $this->input->post('address'),
				'mobile' => $this->input->post('mobile'),
				'gender' => $this->input->post('gender'),
				'status'=> '1'
			);

		$sql = $this->Master_model->common_insert('customer', $data);

		if($sql) {
			$this->session->set_flashdata('msg', 'Customer Successfuly Added');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		redirect($_SERVER['HTTP_REFERER']);
	}

	function updateCustomer(){
		$id= $this->uri->segment(3);
		$data = array(
				'first_name' => $this->input->post('f_name'),
				'last_name' => $this->input->post('l_name'),
				'address'=> $this->input->post('address'),
				'mobile' => $this->input->post('mobile'),
				'gender' => $this->input->post('gender'),
				'status'=> '1'
			);

		$sql = $this->Master_model->updateMaster($data, 'customer', 'cus_id', $id);

		if($sql) {
			$this->session->set_flashdata('msg', 'Customer Successfuly Updated');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		redirect($_SERVER['HTTP_REFERER']);

	}

	function disableCustomer(){
		$id= $this->uri->segment(3);
		$state= $this->uri->segment(4)=='1'?'0':'1';
		$data = array(
				'status'=> $state
			);

		$sql = $this->Master_model->updateMaster($data, 'customer', 'cus_id', $id);;

		$sql = $this->Master_model->updateMaster($data, 'customer', 'cus_id', $id);

		if($sql) {
			$this->session->set_flashdata('msg', 'Customer Successfuly Deactivated');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		redirect($_SERVER['HTTP_REFERER']);

	}

	function saveSupplier() {
		$data = array(
				'first_name' => $this->input->post('f_name'),
				'last_name' => $this->input->post('l_name'),
				'address'=> $this->input->post('address'),
				'gender' => $this->input->post('gender'),
				'status'=> '1'
			);

		$sql = $this->Master_model->common_insert('supplier', $data);

		if($sql) {

		} else {

		}
		redirect($_SERVER['HTTP_REFERER']);

	}

	function updateSupplier() {
		$id= $this->uri->segment(3);
		$data = array(
				'first_name' => $this->input->post('f_name'),
				'last_name' => $this->input->post('l_name'),
				'address'=> $this->input->post('address'),
				'gender' => $this->input->post('gender'),
				'status'=> '1'
			);

		$sql = $this->Master_model->updateMaster($data, 'supplier', 'sup_id', $id);

		if($sql) {

		} else {

		}
		redirect($_SERVER['HTTP_REFERER']);

	}

	function disableSupplier(){
		$id= $this->uri->segment(3);
		$state= $this->uri->segment(4)=='1'?'0':'1';
		$data = array(
				'status'=> $state
			);

		$sql = $this->Master_model->updateMaster($data, 'supplier', 'sup_id', $id);;

		if($sql) {

		} else {

		}
		redirect($_SERVER['HTTP_REFERER']);

	}

	function saveRawmaterial() {
		$data = array(
			'raw_material_name' => $this->input->post('rawmaterial_name'),
				'raw_material_quantity'=> $this->input->post('qty'),
				'raw_price' => $this->input->post('price'),
				'is_active' => '1',
				);

		$sql = $this->Master_model->common_insert('raw_material', $data);

		if ($sql) {
			
		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	function updateRawmaterial(){
		$id= $this->uri->segment(3);
		$data = array(
			'raw_material_name' => $this->input->post('rawmaterial_name'),
				'raw_material_quantity'=> $this->input->post('qty'),
				'raw_price' => $this->input->post('price'),
				'is_active' => '1',
				);

		$sql = $this->Master_model->updateMaster($data, 'raw_material', 'raw_material_id', $id);

		if ($sql) {
			
		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	function disableRawmaterial(){
		$id= $this->uri->segment(3);
		$state= $this->uri->segment(4)=='1'?'0':'1';

		$data = array(
				'is_active' => $state,
				);

		$sql = $this->Master_model->updateMaster($data, 'raw_material', 'raw_material_id', $id);

		if ($sql) {
			
		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);
	}

	function saveProduct() {

		$products = $this->Master_model->getlastproduct();
		$pro_code = "ITM0001";

		if($products != NULL) {
			$num = $products[0]->pro_id + 1;
			$code = str_pad($num, 4, '0', STR_PAD_LEFT);
			$pro_code = "ITM".$code;
		}

		$data = array(
				'pro_code' => $pro_code, 
				'pro_name' => $this->input->post('product_name'), 
				'pro_type' => $this->input->post('type'), 
				'brand_name' => $this->input->post('brand_name'), 
				'guage' => $this->input->post('guage'), 
				'unit_type' => $this->input->post('unit_type'), 
				'unit' => $this->input->post('unit'), 
				'Description' => $this->input->post('description'), 
				'cost' => $this->input->post('cost'), 
				'price' => $this->input->post('price'), 				
				'status' => '1'
			);

		$sql = $this->Master_model->common_insert('products', $data);

		if($sql) {
			$this->session->set_flashdata('msg', 'Product Successfuly Added');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		
		redirect($_SERVER['HTTP_REFERER']);

	}

	function updateProducts() {
		$id = $this->input->post('pro_id');
		$data = array(
				'pro_name' => $this->input->post('product_name'), 
				'pro_type' => $this->input->post('type'), 
				'brand_name' => $this->input->post('brand_name'), 
				'guage' => $this->input->post('guage'), 
				'unit_type' => $this->input->post('unit_type'), 
				'unit' => $this->input->post('unit'), 
				'Description' => $this->input->post('description'), 
				'cost' => $this->input->post('cost'), 
				'price' => $this->input->post('price'), 
				'status' => '1'
			);

		$sql = $this->Master_model->updateMaster($data, 'products', 'pro_id', $id);

		if($sql) {
			$this->session->set_flashdata('msg', 'Product Successfuly Updated');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		
		redirect($_SERVER['HTTP_REFERER']);

	}

	function disableProducts() {
		$id= $this->uri->segment(3);
		$state= $this->uri->segment(4)=='1'?'0':'1';
		$data = array(
				'status' => $state
			);

		$sql = $this->Master_model->updateMaster($data, 'product', 'pro_id', $id);

		if($sql) {
			$this->session->set_flashdata('msg', 'Product Successfuly Disabled');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		
		redirect($_SERVER['HTTP_REFERER']);

	}

	function saveEquipment() {
		$data = array(
				'equipment_name' => $this->input->post('equi_name'),
				'qty' => $this->input->post('qty'), 
				'status' => '1'
			);

		$sql = $this->Master_model->common_insert('equipment', $data);

		if ($sql) {
			
		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);

	}

	function updateEquipment() {
		$id= $this->uri->segment(3);
		$data = array(
				'equipment_name' => $this->input->post('equi_name'),
				'qty' => $this->input->post('qty'), 
				'status' => '1'
			);

		$sql = $this->Master_model->updateMaster($data, 'equipment', 'eq_id', $id);

		if ($sql) {
			
		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);

	}

	function disableEquipment() {
		$id= $this->uri->segment(3);
		$state= $this->uri->segment(4)=='1'?'0':'1';
		$data = array(
				'status' => $state
			);

		$sql = $this->Master_model->updateMaster($data, 'equipment', 'eq_id', $id);

		if ($sql) {
			
		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);

	}

	function saveRegistration() {
		$data = array(
				'full_name'  => $this->input->post('full_name'),
				'mobile'  => $this->input->post('mobile'),
				'username'  => $this->input->post('username'),
				'password'  => md5($this->input->post('password')),
				'status' => '1'
			);

		$sql = $this->Master_model->common_insert('user', $data);

		if($sql) {
			$this->session->set_flashdata('msg', 'User Successfuly Added');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		
		redirect($_SERVER['HTTP_REFERER']);		
	}

	function saveGRN() {

		$data = array(
				'grn_date' => $this->input->post('grn_date'),
				'invoice_no' => $this->input->post('inv_no'),
				'grnd_total' => $this->input->post('txt_grand'), 
				'discount' => $this->input->post('discount'), 
				'net_total' => $this->input->post('net_total'),
				'status' => "1"
			);

		$sql = $this->Master_model->common_insert('grn_detail', $data);

		if ($sql) {

			$last = $this->db->insert_id();
			$pro_id = $this->input->post('table_product');
			$qty = $this->input->post('table_qty');
			$buying_price = $this->input->post('table_buy_price');

			$data_items = array();

			foreach ($pro_id as $key => $value) {
				array_push($data_items, $data = array(
						'pro_id' => $value, 
						'grn_detail_id' => $last,
						'qty' => $qty[$key], 
						'buying_price' => $buying_price[$key]
					));

				$rawMet = $this->Master_model->common_select('raw_material', 'raw_material_id', $value);
				$currQty = $rawMet[0]->raw_material_quantity;

				$newQty = $currQty + $qty[$key];

				$this->db->where('raw_material_id', $value);
				$this->db->update('raw_material', array('raw_material_quantity' => $newQty));

			}

			$this->db->insert_batch('grn_items', $data_items); 


		} else {
			
		}
		
		redirect($_SERVER['HTTP_REFERER']);			
	}

	function saveProduction() {
		$data = array(
				'product_id' => $this->input->post('product'), 
				'production_date' => $this->input->post('production_date'), 
				'endQty' => $this->input->post('end_qty'), 
				'grnd_total' => $this->input->post('txt_pro_grand'), 
				'product_cost' => $this->input->post('pro_product_cost'), 
				'product_margin' => $this->input->post('txt_pro_product_tax'), 
				'product_value' => $this->input->post('pro_product_value'), 
				'status' => '1'
			);

		$sql = $this->Master_model->common_insert('production', $data);

		if($sql) {

			$raw_id = $this->input->post('raw_id');
			$batch_id = $this->input->post('batch_id');
			$price = $this->input->post('price_id');
			$qty = $this->input->post('pro_qty_id');
			$sub_total = $this->input->post('sub_total_id');

			$last = $this->db->insert_id();

			$insert_arr = array();
			foreach ($raw_id as $key => $value) {
				array_push($insert_arr, 			
					$data_item = array(
						'raw_met_id' => $value, 
						'production_id' => $last,
						'batch' => $batch_id[$key], 
						'price' => $price[$key], 
						'qty' => $qty[$key], 
						'sub_total' => $sub_total[$key]
					));
			}

			$this->db->insert_batch('production_row_met', $insert_arr); 

		} else {

		}

		redirect($_SERVER['HTTP_REFERER']);	
	}

	function saveCategory() {
		$data = array(
				'categories'  => $this->input->post('category_name'),
				'status'  => $this->input->post('status')
			);

		$sql = $this->Master_model->common_insert('categories', $data);

		if($sql) {
			$this->session->set_flashdata('msg', 'Category Successfuly Added');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		
		redirect($_SERVER['HTTP_REFERER']);		
	}

	function saveUnit() {
		$data = array(
				'unit_name'  => $this->input->post('unit_name'),
				'status'  => $this->input->post('status')
			);

		$sql = $this->Master_model->common_insert('unit', $data);

		if($sql) {
			$this->session->set_flashdata('msg', 'Unit Successfuly Added');
		} else {
			$this->session->set_flashdata('msg_error', 'Data Adding error');
		}
		
		redirect($_SERVER['HTTP_REFERER']);			
	}

	function saveIssueNote() {
		$data = array(
				'issue_date'  => $this->input->post('date'),
				'from_stock'  => $this->input->post('from_stock'),
				'to_stock'  => $this->input->post('to_stock'),
				'remark' => $this->input->post('remark')
			);

		$this->db->insert('issue_note', $data);
		$id = $this->db->insert_id();

		$pro_id = $this->input->post('line_pro_id');

		$data_detail = array();
		$data_stocks = array();

		foreach ($pro_id as $key => $value) {

			$pro_detials = $this->Master_model->common_select('products', 'pro_id', $value);

			array_push($data_detail, array(
						'pro_id' => $value,
						'issue_note_id' => $id,
						'qty' => $this->input->post('line_qty')[$key],
						'selling_price' => $this->input->post('selling_price')[$key],
						'cost' => $pro_detials[0]->cost,
						'sub_total' => $this->input->post('sub_total')[$key],
					));

			array_push($data_stocks, array(
				'product_id' => $value, 
				'qty' => $this->input->post('line_qty')[$key], 
				'location' => $this->input->post('to_stock'), 
				'st_type' => '1'
			));
		}


		$sql = $this->Master_model->issueNote($data, $data_detail, $data_stocks);

		
		redirect(site_url('view-issue-note/'). $id );	

	}

	function saveSalesNote() {
		$data = array(
				'sd_date' => $this->input->post('date'), 
				'sales_from' => $this->input->post('from'), 
				'customer' => $this->input->post('to'), 
				'remark' => $this->input->post('remark'), 
				'grnd_total' => $this->input->post('grnd_total'),
				'discount' => $this->input->post('discount'),
				'status' => '1'
			);

		$this->db->insert('sale_note', $data);
		$id = $this->db->insert_id();

		$pro_id = $this->input->post('line_pro_id');

		$data_detail = array();
		$data_stocks = array();

		foreach ($pro_id as $key => $value) {

			$pro_detials = $this->Master_model->common_select('products', 'pro_id', $value);

			array_push($data_detail, array(
						'sale_id' => $id,
						'pro_id' => $value,
						'qty' => $this->input->post('line_qty')[$key],
						'selling_price' => $this->input->post('selling_price')[$key],
						'cost_price' => $pro_detials[0]->cost,
						'sub_total' => $this->input->post('sub_total')[$key],
					));

			array_push($data_stocks, array(
				'product_id' => $value, 
				'qty' => '-'.$this->input->post('line_qty')[$key], 
				'location' => $this->input->post('to'), 
				'st_type' => '2'
			));
		}


		$sql = $this->Master_model->salesNote($data, $data_detail, $data_stocks);

		
		redirect(site_url('view-sales-note/'). $id );			
	}

	function saveCSV() {
        $config['upload_path']          = './assets/uploads/';
        $config['allowed_types']        = 'csv|CSV';
        $config['max_size']             = 10000;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('csv_file')) {

                $data = array('upload_data' => $this->upload->data());
                $filename = $data['upload_data']['file_name'];

                $csv = array_map('str_getcsv', file(site_url().'assets/uploads/'.$filename));

                // die(var_dump($csv));
            	$upload_bach = array();

            	// get last product id
				$products = $this->Master_model->getlastproduct();
				$num = 1;

				if($products != NULL) {
					$num = $products[0]->pro_id;
					$code = str_pad($num, 4, '0', STR_PAD_LEFT);
					$pro_code = "ITM".$code;
				}

            	foreach ($csv as $key => $value) {
            		if($key != 0) {
						$code = str_pad($num, 4, '0', STR_PAD_LEFT);
						$pro_code = "ITM".$code;


						// find product Type
						$product_type = $this->Master_model->common_select('categories', 'categories', $value[2]);

						if(count($product_type)<1) {
							
							// insert product Type
							$data_pt = array(
											'categories' => $value[2], 
											'status' => '1'
										);
							$this->db->insert('categories', $data_pt);
							$pro_t_id = $this->db->insert_id();

						} else {
							$pro_t_id = $product_type[0]->cat_id;
						}

						// find unit type
						$unit_type = $this->Master_model->common_select('unit', 'unit_name', $value[4]);
						if(count($unit_type)<1) {
							// insert unit Type

							// insert product Type
							$data_ut = array(
											'unit_name' => $value[4], 
											'status' => '1'
										);
							$this->db->insert('unit', $data_ut);
							$unit_t_id = $this->db->insert_id();

						} else {
							$unit_t_id = $unit_type[0]->unit_id;
						}

						// data set
							$colomns = array(
								'pro_code' => $pro_code, 
								'pro_name' => $value[1], 
								'pro_type' => $pro_t_id, 
								'brand_name' => '', 
								'guage' => $value[3], 
								'unit_type' => $unit_t_id, 
								'unit' => '', 
								'Description' => $value[6], 
								'cost' => $value[7], 
								'price' => $value[8], 
								'status' => '1'
							);
						// data set


						// upload arry
						array_push($upload_bach, $colomns);
						// at last
						$num++;
            		}
            	}

            	$sql = $this->db->insert_batch('products', $upload_bach);


        } else {
            $error = array('error' => $this->upload->display_errors());
            echo var_dump($error);
            $this->session->set_flashdata('msg', 'error,Add Attendance file invalid');
        }

        $this->session->set_flashdata('msg', 'success,CSV upload Success');
        redirect($_SERVER['HTTP_REFERER']);	
	}

	function editProduct() {

	}
}