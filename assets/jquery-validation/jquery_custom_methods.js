$(document).ready(function () {
    
    $("#customer_form").validate({
        rules: {
            f_name: "required",
            l_name: "required",
            address: "required",
            mobile: "required"
        },
        messages: {
            f_name: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Please input First Name",
            l_name: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Please input Last Name",
            address: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Please input Address",
            mobile: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Please input Mobile No"
        },
        // errorLabelContainer: "#summary .error_element",
    }); 


    $("#category_form").validate({
    	rules: {
            category_name: "required",
            status: "required",
    	},
    	messages: {
            category_name: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Please input Category",
            status: "<i class='fa fa-exclamation-triangle' aria-hidden='true'></i> Please input Status",
    	}
    });

});